ansible-midpoint
=========

Sets up new installation of a single or multi-node Midpoint 3.4.1 server(s). Others version not tested, upgrade(s) not supported.

The final deployment contains Apache as a Webserver (80/443), tomcat + deployed midpoint WAR file (8080/tcp) and connects to an external MySQL-like database.

The symetric key will be generated and can be accessed at the end of the *first* play via `midpoint_jceks` - is is actually the base64 representation of the file (the JCEKS keystore itself)

Requirements
------------

* Installed DB server (with a database and user/password with database.* rights)
* Ansible >= 2.3

Known problems
--------------

* ``midpoint_key_pass`` has to be `midpoint` - is hardwired into Midpoint source
* When initializing DB for the first time (running midpoint for the first time again blan DB), you always have to make sure only ONE midpoint is running at a time (https://jira.evolveum.com/browse/MID-3715). Otherwise DB init will fail and Midpoint will be unavailable.

Role Variables
--------------

* `ssl_generic_name` - SSL hostname at which this instance will be available, defaults to FQDN. SSL certificate is expected to live in `/etc/ssl/certs/{{ ssl_generic_name }}.pem` (key respectively in `/etc/ssl/private/`) in one-node setups, otherwise FQDN should be used in multi-node setups with LBs.
* `midpoint_seckey_alias` - Alias for symmetric key in keystore.jceks used by midPoint. Not very important to change. Defaults to `strong`
* `midpoint_store_pass` - Password for the Java keystore `keystore.jceks`. Mandatory, no defaults.
* `midpoint_key_pass` - Password for symmetric key used by midPoint. At the moment, defaults to `midpoint` and if you change it, you will break the installation.


* midpoint_home_directory: the directory where all things midpoint are stored (war, keystore, etc). Default `/var/opt/midpoint`
* midpoint_jceks_filename: the name of the keystore file. Default `keystore.jceks`
* midpoint_jceks_ca_path: the path where certificates will be picked from and imported into JCEKS keystore, default `/usr/local/share/ca-certificates`

* `ssl_ca_chain_http` - CA certificate chain file for HTTP communication   
* `midpoint_mysql_connector_java` - MySQL driver version (defaults to `5.1.39`)
* `midpoint_version` - midPoint version (defaults to `3.4.1`, no other option for now)
* `midpoint_freeipa_connector_version` - FreeIPA connector version. Mandatory, no default


* `midpoint_db_host` - IP/name of the host where to connect to create DB. Mandatory, no default
* `ssl_ca_chain_sql` - CA certificate chain file for SQL connection. Mandatory, no default
* `midpoint_use_sql_ssl` - true if communication between midPoint and DB server uses SSL, otherwise false (default)

* `domain_name` - Domain name for the deployment. URL for redirection is constructed as `midpoint.{{ domain_name }}` and `no_proxy` is set for this domain.
* `ansible_fqdn` - FQDN
* `ansible_hostname` - hostname
* `midpoint_db_admin_username` - SQL DB username for DB deployment
* `midpoint_db_admin_password` - SQL DB password
* `midpoint_db_username` - SQL DB username for DB access by midpoint app
* `midpoint_db_password` - SQL DB password
* `midpoint_jmx_password` - midPoint JMX password (used by midPoint cluster). Mandatory, no default

* `midpoint_http_proxy` - IP address of the HTTP proxy (mandatory, no default value)
* `midpoint_http_port` - port number of the HTTP proxy (mandatory, default `3128`)
* `disable_ansible_log` - disable printing sensitive log data. Default: `true`
* 
Dependencies
------------


Example Playbook
----------------

TBD

License
-------


Author Information
------------------

Attila Szlovak
Michal Medvecky
Diogenes Santos de Jesus

